$(document).ready(function() {
  menu_scroll();
  $('.sidenav').sidenav();
  $('.parallax').parallax();
  $('.scrollspy').scrollSpy();
});


function menu_scroll() {
  var controleNav = false;

  $(document).scroll(function(e) {
    var scrollTop = $(document).scrollTop();
    if (scrollTop > 100) {
      if (controleNav == false) {
        $('nav').first().removeClass('transparent');
        controleNav = true;
      }
    } else {
      if (controleNav == true) {
        $('nav').first().addClass('transparent');
        controleNav = false;
      }
    }
  });

}
