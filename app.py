from flask import Flask, render_template
from os import listdir

app = Flask(__name__)


def list_users():
    users = listdir('./templates/users')
    return users


@app.route('/')
def index():
    return render_template('index.html', users=list_users(), script=True)


if __name__ == '__main__':
    app.run()
